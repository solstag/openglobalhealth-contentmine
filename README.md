Code work related to my ContentMine Fellowship around Open Global Health.

See the research notebook at:

<https://en.wikiversity.org/wiki/Open_Global_Health/ContentMine>

# Scripts

mesh2cmdict.py - Creates CM compatible dictionaries from MeSH definitions
